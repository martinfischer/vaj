local current_dir='${PWD/#$HOME/~}'
local git_info='$(git_prompt_info)'
local hg_info='$(hg_prompt_info)'

PROMPT="%{$terminfo[bold]$FG[202]%}[SSH]%{$reset_color%} %{$terminfo[bold]$FG[040]%}%n%{$reset_color%} %{$terminfo[bold]$FG[240]%}on%{$reset_color%} %{$terminfo[bold]$FG[033]%}%m%{$reset_color%} %{$terminfo[bold]$FG[240]%}in%{$reset_color%} %{$terminfo[bold]$FG[226]%}${current_dir}%{$reset_color%}${git_info}${hg_info} %{$terminfo[bold]$FG[135]%}>>%{$reset_color%} "

RPROMPT="%{$terminfo[bold]$FG[240]%}[%D{%d.%m.%Y - %H:%M:%S}]%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$terminfo[bold]$FG[240]%}git%{$reset_color%} %{$fg[255]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$FG[202]%}✘"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$FG[040]%}✔"

ZSH_THEME_HG_PROMPT_PREFIX=" %{$terminfo[bold]$FG[240]%}hg%{$reset_color%} %{$fg[255]%}"
ZSH_THEME_HG_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_HG_PROMPT_DIRTY="%{$FG[202]%}✘"
ZSH_THEME_HG_PROMPT_CLEAN="%{$FG[040]%}✔"

